



<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" xmlns:og="http://opengraphprotocol.org/schema/" xmlns:fb="http://www.facebook.com/2008/fbml">
  <head>
<!--TTGP071092260695-->
        
 <meta property="fb:admins" content="590151829, 652140569, 713167050, 658585477, 549821563, 657746566,100001642472689" />
        <meta property="fb:app_id" content="158634007495794" />
 <meta property="og:site_name" content="Jobsite" />
 <meta property="og:image" content="http://www.jobsite.co.uk/m/images/jobsite_logo.gif" />
 <meta property="og:type" content="website" />
 <meta property="og:title" content="Change Manager Job in City Of London Jobsite" />
 <meta property="og:description" content="Salary: Competitive salary + benefits, Job Type: Permanent, Duration: , Start Date: ASAP - Apply Now for this Change Manager vacancy!" />  
 <meta property="og:url" content= "http://www.jobsite.co.uk/job/change-manager-957209288" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge, Chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="msvalidate.01" content="B91F58995718C82B65D991BA2D6CAA07" />
<script>var traceStartTime = new Date();</script>

    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>
    <link href="/css/frame.css?v=657c3a " rel="stylesheet" type="text/css">
    
    <link href="/css/core/generic_css.css?v=657c3a " rel="stylesheet" media="screen, projection">
    <link href="/css/responsive.css?v=657c3a " rel="stylesheet" media="screen, projection">
    <link href="/css/core/advancedsearch.css?v=657c3a " rel="stylesheet" media="screen, projection">
	
  <script src="/js/jquery-latest.min.js"></script>
  <script src="/js/jquery.cookie.js"></script>

  <link href="/css/fonts.css" rel="stylesheet" type="text/css">
  <link href="/css/header.css" rel="stylesheet" type="text/css">


  <!--[if lt IE 9]>
    <link href="/css/ie8.css" rel="stylesheet" type="text/css">
    <script src="/js/html5shiv.min.js"></script>
    <script src="/js/respond.min.js"></script>
    <style>nav{display:none;}</style>
  <![endif]-->
    
<!--  Maxymiser  script  start  -->
<script  type="text/javascript"
  src="//service.maxymiser.net/cdn/jobsite/js/mmcore.js"></script>
<!--  Maxymiser  script  end  -->


<script>

  var  _gaq = _gaq || [];
  _gaq.push(['_setAccount',  'UA-360555-1']);
  _gaq.push(['_trackPageview']);

  (function()  {
     var ga = document.createElement('script'); ga.type =  'text/javascript'; ga.async = true;
     ga.src = ('https:' ==  document.location.protocol ? 'https://' : 'http://') +  'stats.g.doubleclick.net/dc.js';
     var s =  document.getElementsByTagName('script')[0];  s.parentNode.insertBefore(ga, s);
   })();

</script>

<title>Change Manager Job in City Of London Jobsite</title>
<!--  BEGIN Krux Control Tag for "JobSite" --><!-- Source:  /snippet/controltag?confid=KHjuay_L&site=JobSite&edit=1  --><script class="kxct" data-id="KHjuay_L"  data-timing="async" data-version="1.9"  type="text/javascript">  window.Krux||((Krux=function(){Krux.q.push(arguments)}).q=[]);  (function(){     var  k=document.createElement('script');k.type='text/javascript';k.async=true;     var  m,src=(m=location.href.match(/kxsrc=([^&]+)/))&&decodeURIComponent(m[1]);     k.src =  /^https?:\/\/([a-z0-9_\-\.]+\.)?krxd\.net(:\d{1,5})?\//i.test(src)  ? src : src === "disable" ? "" :       (location.protocol==="https:"?"https:":"http:")+"//cdn.krxd.net/controltag?confid=KHjuay_L"  ;     var  s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(k,s);  }());  </script><!--  END Krux Controltag -->
<script src="//assets.adobedtm.com/001951d04f0f0e0f889f72b0546583f37849391c/satelliteLib-b577d63d93a2398e37b19efc144b968c1bb173e4.js"></script>

<script>if(!window.jss||!window.jss.third)document.write('<script   src="\/js\/core\/third.js"><\/script>');</script>

<script>
var digitalData = {
	pageName: "Listing Page - Semi Display",
	protocol: "http",
	domain: "www.jobsite.co.uk",
	url: "http://www.jobsite.co.uk" + window.location.pathname,
	siteCode: "UK",
	visitorStatus: "anonymous",
	visitorType: "",
	listListingId: "957209288",
	listingSector: "IT",
	agencyId: "295838",
	agencyName: "Sogeti Uk Ltd",
	agencyPhone: "not present",
	searchResults: "",
	otherJobsType: "",
	normalisedJobTitle: "Change Manager",
	discipline: "IT",
	subDiscipline: "Project Management",
	applicationType: "external application form|capture",
	browserCheck: "D=User-Agent"
}

if ($.cookie('aaseg')) {
	const aaSeg = $.cookie('aaseg').split('|');
	if (aaSeg.length === 4) {
		digitalData.cookieND = aaSeg[0];
		digitalData.cookieNSD = aaSeg[1];
		digitalData.cookieJT = aaSeg[2];
		digitalData.cookieNS = aaSeg[3];
	}
}

</script>

    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
    <meta name="description" content="Salary: Competitive salary + benefits, Job Type: Permanent, Duration: , Start Date: ASAP - Apply Now for this Change Manager vacancy!">

    <link rel="canonical" href="http://www.jobsite.co.uk/job/change-manager-957209288">
    <link rel="stylesheet" href="/css/vacancies.css">

    




<style>
.branded .vacancySection.heading,
.branded .vacancySection.title,
.branded .vacancySection.recruiter-info,
.branded .vacancySection.recruiter-info a {
  background-color: #df371a;
  color: #ffffff;
}
.branded .vacancySection.recruiter-info a {
  font-weight: inherit;
  text-decoration: underline;
}
.branded .vacancySection.recruiter-info a.phone {
  text-decoration: none;
}
.jbecontent.emailsFeature.jbeRight {
  border-color: #df371a;
}
.branded .vacancySection.background {
  
  background-color: #e5e5e5;
  background-repeat: repeat;
  background-image: url('/semilogos/bgs/pattern_blocky.png');
  
}
.branded .workFor h1,
.branded .workFor small,
.vacancy.branded #content .workFor h1,
.vacancy.branded #content .workFor small {
  
}
.branded .vacancySection.title .applynowButton {
  border: 1px solid #ffffff;
  background: #df371a;
  color: #ffffff;
}
.branded .vacancySection .call-button {
  border: 1px solid #ffffff;
  background: #ffffff;
  color: #df371a;
}
.branded .vacancySection .call-button a {
  color: #df371a;
  background: #ffffff;
}
.branded .vacancySection.title .applynowButton:hover {
  background: #ffffff;
  color: #df371a;
}
.branded .overlay{
  
}
.branded .agencyLogo {
  width: 185px;
  border: 1px solid #df371a;
  max-width: 250px;
}
#semiLogo {
  width: 100%;
  display: block;
  border: 8px solid #fff;
}

@media screen and (max-width: 768px) {
  .branded .overlay {
    position: relative;
  }
}

@media screen and (max-width: 460px) {
  .branded .vacancySection.title .applynowButton {
    background: #008fb3;
    color: #fff;
  }
  .white .saved-job {
    
  }
  .white .saved-job.hover {
    
  }
  .white .saved-job.saved {
    
  }

}
</style>


    <script src="/js/core.js"></script>
    <script src="/js/controls.js"></script>


<script type="text/javascript" src="//static.criteo.net/js/ld/ld.js" async="true"></script>
<script type="text/javascript">
	window.criteo_q = window.criteo_q || [];
	window.criteo_q.push(
	        { event: "setAccount", account: '9045'},
	        { event: "setHashedEmail", email: ""}
	);

	if(jss.browser.mobile == true){
		window.criteo_q.push({event: "setSiteType", type: "m"});
	}
	else if(jss.browser.tablet == true){
		window.criteo_q.push({event: "setSiteType", type: "t"});
	}
	else {
		window.criteo_q.push({event: "setSiteType", type: "d"});
	};

</script>

<script type="text/javascript">
    window.criteo_q.push({event: "viewItem", item: "957209288"});
</script>    
<style type="text/css">
  .jsMoreLocations {white-space:nowrap;display:none;}
</style>
<script>
  $(document).ready(function(){
    var searchedTerm = $('.location .tag').first().clone().children().remove().end().text(),
        locationElement = $('.jsLocationList'),
        locationToggle = $('.jsMoreLocations'),
        lessLocations = [],
        moreLocations = [],
        cleanLocations = locationElement.first().text().split(','),
        defaultSize = 3,
        locationsSize = cleanLocations.length;

    // Check if locations size is more then default display size
    if(locationsSize > defaultSize) {
        // Attempt to search for the first location
        if(searchedTerm.length) {
          for(var i=0; i<locationsSize; i++) {
            if(searchedTerm.toLowerCase() === cleanLocations[i].trim().toLowerCase() && lessLocations.length === 0) {
              var match = cleanLocations[i];
              cleanLocations.splice(i,1);
              cleanLocations.unshift(match);
            }
          }
        }
        lessLocations = cleanLocations.slice(0, defaultSize).join(', ');
        moreLocations = cleanLocations.slice(defaultSize, locationsSize);
    }
    else {
        lessLocations = cleanLocations.join(', ');
    }
    // Load locations by default
    locationElement.text(lessLocations);

    // Check for more locations and show toggle Link
    if(moreLocations.length) {
      locationToggle.text(' + ' + moreLocations.length + ' more').show();
    }

    $('.jsMoreLocations').on('click', function(e) {
      e.preventDefault();
      if ($(this).hasClass('expanded')) {
        $(this).prev().text(lessLocations);
        $(this).removeClass('expanded').text(' + ' + moreLocations.length + ' more');
      }
      else {
        $(this).prev().text(cleanLocations.join(', '));
        $(this).addClass('expanded').text('Show less');
      }
    });
  });
</script>

  </head>
  <body data-searchref="" class="branded vacancy " data-aa-event="event14">
	<script>document.body.className = document.body.className + ' svg'</script>
<script src="/js/underscore-1.8.3.min.js"></script>
  <nav>
    <a class="close">x</a>
    <ul class="main">
      
        <li><a href="/cgi-bin/login_applicant.cgi?src=qlSignIn">Login</a></li>
        <li><a href="/cgi-bin/job_basket.cgi?sort=expiry_date&amp;src=qlSavedJobs">Saved jobs</a></li>
        <li><a href="/cgi-bin/mj-account.cgi?src=qlRegister">Register</a></li>
        <li><a href="/cgi-bin/mj-account.cgi?src=qlUploadCV">Upload your CV</a></li>
	  		<li><a href="http://www.jobsite.co.uk/worklife">Blog</a></li>
      
    </ul>
    <div class="divider"></div>
    <ul class="main recruiter">
      <li><a href="/recruitment/?src=tab&amp;area=bottom">Recruiters</a></li>
    </ul>
    <div class="divider"></div>
    <ul class="sub">
      <li><a href="/about-us">About us</a></li>
      <li><a href="/work-for-us">Work for us</a></li>
      <li><a href="/mobile">Jobsite apps</a></li>
      <li><a href="/faq/Jobseeker">Help</a></li>
      <li><a href="/contact">Contact us</a></li>
      <li><a href="/site_map.html">Site map</a></li>
    </ul>
    <div class="divider"></div>
    <div class="social overflow">
      <a href="//twitter.com/jobsiteuk" class="overflow"></a>
      <a href="//facebook.com/JobsiteUK" class="overflow"></a>
      <a href="//plus.google.com/u/0/107538137306798419802/posts" class="overflow"></a>
      <a href="//youtube.com/user/JobsiteUK" class="overflow"></a>
    </div>
  </nav>
  <div class="container">
  <div class="recruiter-strip">
		<div class="center overflow">
			<div id="headerBanner" class="disblock"></div>
        <a href="/recruitment/products"><button class="yellow">Post a job</button></a>
		</div>
  </div>
  <header>
    <menu class="overflow">
      <button class="menu blue">Menu</button>
      <a href="/"><div class="logo-small"></div></a>
      <div class="hamburger">
        <span></span>
        <span></span>
        <span></span>
      <div id="mobileBadge" class="mobile-badge"></div>
      </div>
			<div class="mobile-search"></div>
      
        <a href="/cgi-bin/login_applicant.cgi?src=qlSignIn"><button class="login blue">Login</button></a>
      
    </menu>
		<div class="form-wrapper overflow">
			<form method="get" action="/vacancies" id="psformnew" name="psformnew" class="overflow">
        <input type="hidden" name="search_type" value="quick">
        <input type="hidden" name="query" id="ts_fp_skill_include" value="">
        <input type="hidden" name="location" id="ts_location_include" value="">

				<div class="jobTitle search-item overflow">
					<div class="tag-container overflow">
						<ul class="overflow">
							<li class="new"><input id="jobTitle-input" name="jobTitle-input" type="text" autocomplete="off" tabindex="1" size="1"></li>
						</ul>
					</div>
					<label>Job title or skill</label>
          <div class="output typeahead">
            <ul class="overflow"></ul>
          </div>
				</div>

				<div class="location search-item overflow">
					<div class="tag-container overflow">
						<ul class="overflow">
							<li class="arrow"><a class="crosshair" href="#"></a></li>
							<li class="new"><input id="location-input" name="location-input" type="text" class="hidden" autocomplete="off" tabindex="2" size="1"></li>
						</ul>
					</div>
					<label>Location</label>
					<div class="output typeahead">
						<ul class="overflow"></ul>
					</div>
				</div>
				<div class="distance search-item overflow">
					<div class="tag-container overflow">
						<select tabindex="3" data-ga-label="location_within" id="ts_location_within" name="radius" class="distance">
							<option value="3">3 miles</option>
							<option value="5">5 miles</option>
							<option value="10">10 miles</option>
							<option value="15">15 miles</option>
							<option selected="selected" value="20">20 miles</option>
							<option value="30">30 miles</option>
							<option value="50">50 miles</option>
						</select>
					</div>
					<label>Distance</label>
				</div>
				<div class="button search-item overflow">
					<button type="submit" id="searchBtn" alt="Search" tabindex="4">Search</button>
				</div>
			</form>
		</div>
  </header>
<div id="page">
  <div class="middleContainer" id="middleContainer">
    <div id="jsMessages"></div>
    <div class="wrapper contentWrapper">



    
    <main>
      <div class="vacancySection heading">
        <div class="wrap">
          <div class="left">
            <a href="javascript:history.back();" class="backToSearch"><b>Back <span class="mobile-hide">to results</span></b></a>
          </div>
          <div class="right">
            <b><span class="mobile-hide">Job </span>Posted: </b>21 Dec 2016
          </div>
          <div class="clear"></div>
        </div>
      </div>

      <div class="vacancySection background pattern">
        <div class="overlay">
          <div class="wrap">
            <div class="workFor">
              <small class="mobile-hide">Work as</small>
              <div class="save vacancy-save job mobile-save">
                <div class="img saved-job btn jobSave track" data-id="957209288"></div><a href="#" class="status mobile-hide">Save Job</a>
              </div>
              <h1>Change Manager</h1>
            </div>
            
            <div class="agencyLogo">
               <a  href="/cgi-bin/agencyinfo.cgi?agency_id=295838"><img src="/semilogos/sogeti-ukl.gif" id="semiLogo" alt=""></a>
            </div>
            
          </div>
        </div>
      </div>

      <div class="vacancySection title">
        <div class="wrap">
          <div class="left summary">
            <h2>
              <span class="salary">Competitive salary + benefits</span><span class="divider">|</span>
              <span class="locationConcat">City Of London</span><span class="divider">|</span>
              <span class="jobType">Permanent</span>
            </h2>
          </div>
          <div class="right apply">
            
              <a href="        https://www.jobsite.co.uk/cgi-bin/applynow.cgi?ord=D&vac_ref=957209288&tracking_src=search&tmpl=sem&sctr=IT&skip_login=0&intcid=&engine=stepmatch&search_referer=internal" class="button applynowButton">Apply Now</a>
            
            <div class="right save vacancy-save job">
              <div class="img saved-job btn jobSave track" data-id="957209288"></div>
              <a href="#" class="status mobile-hide">Save Job</a>
            </div>
          </div>
          <div class="clear"></div>
        </div>
      </div>
      <div class="clear"></div>

      <div class="vacancySection content">
        <div class="description left">
          <p><strong>Change Manager � Sogeti UK</strong></p>
<p><strong>Location � City of London</strong></p>
<p><strong>Accelerate your career in our world leading IT consultancy</strong></p>
<p>Sogeti UK is part of the Sogeti Group, which brings together more than 20,000 professionals in 15 countries and is present in over 100 locations in Europe, the US and India.</p>
<p>Sogeti is one of the five strategic business units of Capgemini Group, recognised by analysts for our focus on transformation, our methodologies and frameworks, and our local and global delivery capability.</p>
<p>Sogeti is recognised as a thought leader and leader in delivering testing solutions and in more recent years, diversifying our expertise by building new practices such as DevOps, Release Management and Service Design Transition.</p>
<p>�</p>
<p><strong>The Role</strong></p>
<p>We are looking for a Change Manager to join our Sogeti family and work on our client sites as a consultant. Our customers including some household names across a number of industry sectors, including government, legal, banking, insurance, defence, aerospace and media/digital. For your first project you will be onsite with a global insurance customer.</p>
<p>�</p>
<p><strong>This Change Manger will:-</strong></p>
<ul>
<li>Manage and communicate to wider delivery team any changes to agreed work items.</li>
<li>Manage the project Change Management Library to ensure the integrity of baselines and configuration items.</li>
<li>Build and maintain traceability of releases through JIRA/Confluence on various environments.</li>
<li>Coordinate with the delivery team and test team to understand / define deployment milestones to actively manage with deployment.</li>
<li>Own the central repository to retain packages and builds outputs to be utilised for deployment on various environments.</li>
<li>Creating and carrying out Change Management Reviews as per the Change Management Plan in conjunction with the PM.</li>
<li>Participate in the Change Management Review / Audits as well as reporting on Change Management Reviews.</li>
<li>Resolve Change Management Review / Audit corrective action plans and oversee their implementation.</li>
<li>Coordinate documentation and meetings preferably for GO live deployment as well, this will be supported by wider team on the ground.</li>
</ul>
<p>�</p>
<p><strong>As a Change Manager, we�re looking for:-</strong></p>
<ul>
<li>Previous commercial experience of Change Management.</li>
<li>Great stakeholder management skills.</li>
<li>Previous experience of working in Agile / Scrum environments.</li>
<li>ITIL foundation certified or above.</li>
<li>Flexibility on travel / work locations.</li>
<li>Scrum master certification would be beneficial but is not essential.</li>
</ul>
<p>�</p>
<p><strong>What we can do for you!</strong></p>
<p>We encourage continuous learning and development and you�ll have the ability to obtain further certifications through Sogeti Training.</p>
<p>You�ll also have the opportunity to develop your skill set and domain knowledge across a range of industries including Finance, Public Sector, Professional Services, and Retail to name a few!</p>
<p>�</p>
<p><strong>Benefits</strong></p>
<p>Sogeti offer an additional 5% Flexible Benefits fund that gives our employees the opportunity to elect the benefits that best suit their needs. Each consultant has the choice of:</p>
<p>Core Annual Leave Days, dependent on grade, with the option to buy or sell up to 5 days leave subject to minimum annual entitlements; Life Assurance (mandatory); Long Term Disability Insurance (mandatory); Private Medical Insurance; Dental Cover; Personal Accident Insurance; Childcare Vouchers; Healthcare Vouchers.</p>
<p>You will also have the option to enter into a pension scheme where Sogeti UK contribute 6% and the employee contributes 4% giving a total of 10% (This can be opted out of).</p>
<p>�</p>
<p><strong>Our inclusive Culture</strong></p>
<p>Sogeti UK positively encourages applications from suitably qualified and eligible candidates regardless of sex, race, disability, age, sexual orientation, gender reassignment, religion or belief, marital status, or pregnancy and maternity. We foster an inclusive culture that enables everyone to achieve their full potential and enjoy a fulfilling career with us.</p>
          <div class="clear"></div>
          <div class="bottom-buttons">
            
              <a href="        https://www.jobsite.co.uk/cgi-bin/applynow.cgi?ord=D&vac_ref=957209288&tracking_src=search&tmpl=sem&sctr=IT&skip_login=0&intcid=&engine=stepmatch&search_referer=internal" class="button applynowButton">Apply Now</a>
            
          </div>
        </div>
        <div class="sidebar right">
          
            
  <div class="jbecontent emailsFeature jbeRight clearfix">
    <form class="minJobAlert">
      <div class="jbeContentHolder bottom clearfix">
        <div id="minjbeform1">
          <div class="workingAnim">
            <div class="dot"></div>
            <div class="dot2"></div>
          </div>
          <div class="emailsHeader">Get similar jobs by email</div>
          <input type="email" name="email" class="email jobAlert" placeholder="Enter your email" title="Please enter a valid email address" required>
          <p>Please enter a valid email address</p>
          <input type="submit" class="submit minJobAlertSubmit" value="Send me jobs">
        </div>
        <small>By confirming you accept our <a href="https://www.jobsite.co.uk/applicant_terms" title="Terms &amp; Conditions" target="_blank">terms</a></small>
        <noscript>You must enable Javascript to use our Job Alert signup.</noscript>
      </div>
    </form>
  </div>

          
        <div class="options first">
          <div class="left printButton mobile-hide">
            <div class="img print"></div>
            <a href="javascript:window.print();">Print job</a>
          </div>
          <div class="left vacancy-save save job lg-screen-hide">
            <div class="img saved-job bottom-star btn track" data-id="957209288"></div><a href="#" class="status">Save Job</a>
          </div>
          <div class="right shareJobs">
            <div class="img email"></div>
            <a href="https://www.jobsite.co.uk/cgi-bin/friend.cgi?fr_url=referer&fr_type=vacancy&fr_vacref=957209288" target="_blank">Share job by email</a>
          </div>
        </div>

        <div class="options">
          <p class="rightInfo"><b>Share:</b></p>
          <div class="addthis_sharing_toolbox"></div>
        </div>
        <div class="options vac-ref">
          <p>Ref no: 295838-758</p>
        </div>
        <div id="recruiter-info-old">
          <div class="options">
            <a  href="/cgi-bin/agencyinfo.cgi?agency_id=295838"><img src="/jobsite-gif/295838/295838.gif" class="recruiter-logo" alt=""></a>
          </div>

          <div class="options contact">
            <p class="rightInfo"><b>Recruiter:</b> Sogeti Uk Ltd</p>
            
          </div>

          <div class="options">
            <p><a href="/vacancies?agency_id=295838" class="moreJobsWithRecruiter">View more jobs with recruiter</a></p>
          </div>
        </div>
        </div>
        <div class="clear"></div>
      </div>
      <div id="recruiter-info-new" class="vacancySection recruiter-info">
        
        <div class="column contact">
            
              <a  href="/cgi-bin/agencyinfo.cgi?agency_id=295838"><img src="/jobsite-gif/295838/295838.gif" class="recruiter-logo" alt=""></a>
            
        </div>
        
        <div class="column contact">
          
          <div>
            <span class="email">Email: <a href="mailto:jobs.uk@sogeti.com">jobs.uk@sogeti.com</a></span>
          </div>
        </div>
        <div class="column">
          Recruiter: <span class="recruiter-name">Sogeti Uk Ltd</span><br/>
          <a href="/vacancies?agency_id=295838" class="moreJobsWithRecruiter">View more jobs with recruiter</a>
        </div>
        <div class="column vac-ref">
          Job ref no:<br/>
          295838-758
        </div>
      </div>
      
        <div id="jobStrip" data-jobid="957209288" data-src="listing-semi"></div>
      
    </main>

    
				<div class="disnone" id="sideBanner"> </div>
			</div>
	</div>
</div>

<footer>
	<a href="/cgi-bin/ovp_products.cgi?action=info&amp;service_type=vacancy"><button class="yellow">Post a job</button></a>
	<h2>Already a client? <a href="/cgi-bin/login_consultant.cgi">Recruiter login</a></h2>
	<p>&copy; Jobsite UK (Worldwide) Ltd. All rights reserved.</p>
	<p>
		<a href="/terms_of_use">Terms</a>
		<a href="/privacy">Privacy</a>
		<a href="/cookies">Cookies</a>
		<noscript>
			<a href="/site_map.html">Site map</a>
			<a href="http://www.jobsite.co.uk/worklife">Blog</a>
		</noscript>
	</p>
</footer>
<script src="/js/header.js"></script>
<script>
	var jobTitleTags =  {
		input: getLastSearchCookieCookie('lastJobTitle') || '',
		location: $('.jobTitle .tag-container .new') || ''
	},
	locationTags = {
		input: getLastSearchCookieCookie('lastSearch') || '',
		location: $('.location .tag-container .new') || ''
	},
	origin = 'cookie';

	if(getLastSearchCookieCookie('lastRadius') == "") {
		$('#ts_location_within').val('20')
	} else {
		$('#ts_location_within').val(getLastSearchCookieCookie('lastRadius'));
	}
	function populateSearchBar(){
		typeahead.buildTag(jobTitleTags.input, jobTitleTags.location, origin);
		typeahead.buildTag(locationTags.input, locationTags.location, origin);
		typeahead.closeTag();

		$('form#psformnew > div').each(function(){
			if($('li').hasClass('tag')){
				$('.tag-container', this).removeClass('open');
				$('.arrow a').addClass('white');
			}
		});
	};
	populateSearchBar();
	$(window).on('load resize', function(){
		$('#page').css('min-height',
			$('body').innerHeight() - (
				$('.recruiter-strip:visible').outerHeight() +
				$('footer').outerHeight() +
				$('header').outerHeight() + 40
			)
		);
	});
</script>


</div>



	<script src="/js/responsive.js"></script>
	
		<script>
			$(document).ready(function(){
				$.ajax({ url: "/visitorId" });
			});
		</script>
	
<script src="/js/jquery.cookie.min.js"></script>

<script>
  if(window.jss)
    jss.third.envMod({"state":{"esc_location_include":"PO_EC2M_EC2M","loginstatus":"NotLoggedIn","in_page":"vacdetails","applicant_id":""}});</script>

<script>
var traceSession = '',
    traceParent = '',
    UUID = 0,
    defaultTraceSessionId = 0;

function traceSessionFun(traceSession,traceParent,traceData){
    var label = window.location.href,
        tracePayLoad = {
            "type": "action",
            "parent": traceParent,
            "label": label,
            "bulk": traceData,
        };
    $.ajax({
        type: "POST",
        url: "/trace/" + traceSession,
        data: JSON.stringify(tracePayLoad),
        contentType: "application/json; charset=utf-8",
    }).done(function(res){
        $.cookie('traceSession',res.root + '.' + res.id,{path:'/'});
        
    }).fail(function(jqXHR,textStatus){
        console.log("Trace request failed:");
        console.log(textStatus);
    });
}
$(window).load(function(){
    setTimeout(function(){
        if(typeof traceData!=="undefined"){
            if(typeof traceStartTime!=="undefined")traceData["loadTime"] = window.performance.timing.loadEventEnd - window.performance.timing.navigationStart;
            traceData["domain"]=location.hostname;
                if(!$.cookie('traceSession'))$.cookie('traceSession',UUID,{path:'/'});
                var traceSessionCookie = $.cookie('traceSession'),
                    traceSessionArray = traceSessionCookie.split(".");
                traceSession = traceSessionArray[0];
                if(traceSession == 0)traceData["new_session"] = 1;
                if(traceSessionArray[1]!==undefined){traceParent = traceSessionArray[1]};
                
                traceSessionFun(traceSession,traceParent,traceData);
        }
    },0);
});

function traceAction(data){
    if(!data)return;

    if (!data["bulk"]) {
        actionPayload = {
            "bulk": data
        };
    } else {
        actionPayload = data;
    }

    if(typeof traceStartTime!=="undefined")actionPayload.bulk.time_to_action = new Date() - traceStartTime;
    actionPayload.bulk.domain = location.hostname;
    if(!data.type)actionPayload.type = "action";
        var traceSessionCookie = getTraceCookie();
        if(!traceSessionCookie){
            setTraceCookie(defaultTraceSessionId);
            traceSessionCookie = defaultTraceSessionId;
        }
        var traceSessionArray = traceSessionCookie.split(".");
        traceSession = traceSessionArray[0];
        if(traceSession == defaultTraceSessionId)actionPayload.bulk.new_session = 1;
        actionPayload.parent = traceSessionArray[1];
        $.ajax({
            type: "POST",
            url: "/trace/" + traceSession,
            data: JSON.stringify(actionPayload),
            contentType: "application/json; charset=utf-8",
        })
        .done(function(res){
            setTraceCookie(res.root + "." + res.id);
        });
}

function getTraceCookie(){
    return $.cookie("traceSession");
}

function setTraceCookie(value){
    $.cookie("traceSession", value, {path: "/"});
}
</script>


    <script src="/js/saved-jobs.js"></script>
    <script src="/js/jquery.dotdotdot.js"></script>
    <script src="/js/textfill.min.js"></script>
    <script src="/js/vacancies.js"></script>
    <script src="/js/bootstrap/modal.min.js"></script>

	<script>
		function getUrlParameter(sParam){
			var sPageURL = window.location.search.substring(1),
				sURLVariables = sPageURL.split('&');
			for (var i = 0; i < sURLVariables.length; i++){
				var sParameterName = sURLVariables[i].split('=');
				if (sParameterName[0] == sParam)return sParameterName[1];
			}
		}
		var vacDetails = {};
		vacDetails.pos = getUrlParameter("position"),
		vacDetails.reciprocalRank = 1/vacDetails.pos,
		vacDetails.page = getUrlParameter("page"),
		vacDetails.sector = getUrlParameter("sctr"),
		vacDetails.src = getUrlParameter("src"),
		vacDetails.engine = getUrlParameter("engine") || "",
		vacDetails.search_referrer = getUrlParameter("search_referer") || "",
		vacDetails.sKeywords = "",
		vacDetails.sLocation = "PO_EC2M_EC2M";
		var referrer = document.referrer;
		var matchSolr = /\/vacancies\??/;
		var matchDb = /\/cgi-bin\/advsearch\??/;
		if(referrer.replace(/.*?:\/\//g, "").indexOf(location.host) === 0){
			if(matchSolr.test(referrer)){
				if(vacDetails.engine=="")vacDetails.engine="solr";
				$.removeCookie('oracle',{path:'/',domain:'.www.jobsite.co.uk'});
				$.cookie('searchurl',referrer,{path:'/',domain:'.www.jobsite.co.uk'});
			}else if(matchDb.test(referrer)){
				if(vacDetails.engine=="")vacDetails.engine="db";
				$.removeCookie('jobnod',{path:'/',domain:'.www.jobsite.co.uk'});
			}else{
				$.removeCookie('oracle',{ path:'/',domain:'.www.jobsite.co.uk'});
				$.removeCookie('jobnod',{ path:'/',domain:'.www.jobsite.co.uk'});
			}
		}else{
			$.removeCookie('oracle',{ path:'/',domain:'.www.jobsite.co.uk'});
			$.removeCookie('jobnod',{ path:'/',domain:'.www.jobsite.co.uk'});
		}
		var traceData = {
			"alternative_store": 1,
			"vacancy_ref": "957209288",
			"vacancy_search_position": vacDetails.pos,
			"vacancy_search_reciprocal_rank": vacDetails.reciprocalRank,
			"vacancy_search_page": vacDetails.page,
			"vacancy_search_keywords": vacDetails.sKeywords,
			"vacancy_search_location": vacDetails.sLocation,
			"vacancy_search_sector": vacDetails.sector,
			"vacancy_search_src": vacDetails.src,
			"vacancy_search_referrer": vacDetails.search_referrer
		};
		if(typeof vacDetails.engine!=="undefined")traceData.vacancy_search_engine = vacDetails.engine;
		if(vacDetails.search_referrer==""){
			if(referrer.replace(/.*?:\/\//g, "").indexOf(location.host) !== 0){
				var sReferrer = getUrlParameter("cid") || "",
					utmMedium = getUrlParameter("utm_medium") || "",
					utmSource = getUrlParameter("utm_source") || "",
					googleCID = /seadvert_Google_Search/.test(sReferrer),
					bingCID = /seadvert_Bing_SEARCH/.test(sReferrer),
					aggCID = /mse/.test(sReferrer),
					checkJaCID = /B2C_CLC/.test(sReferrer),
					jobAlertCID = /Ja/.test(sReferrer),
					mailingCID =  /B2C_mailing/.test(sReferrer),
					retargetingCID = /banner_retargeting/.test(sReferrer);
				if(googleCID)traceData.vacancy_search_referrer = "google";
				else if(bingCID)traceData.vacancy_search_referrer = "bing";
				else if(aggCID)traceData.vacancy_search_referrer = "aggregator";
				else if(checkJaCID || jobAlertCID)traceData.vacancy_search_referrer = "jobalert";
				else if(aggCID || utmMedium == "aggregator")traceData.vacancy_search_referrer = "aggregator-" + utmSource;
				else if(mailingCID)traceData.vacancy_search_referrer = "mailing";
				else if(retargetingCID)traceData.vacancy_search_referrer = "retargeting";
				else {
					var yahoo = /https?:.*yahoo.*/;
					var mainReferrers = /(https?:.*google.*)|(https?:.*bing.*)|(https?:.*indeed.*)/;
					if(yahoo.test(referrer))traceData.vacancy_search_referrer = "external-" + referrer.split('/')[2].split('.')[2];
					else if(mainReferrers.test(referrer))traceData.vacancy_search_referrer = "external-" + referrer.split('/')[2].split('.')[1];
					else traceData.vacancy_search_referrer = "external-other";
				}
			}else traceData.vacancy_search_referrer = "internal";
		}
	</script>


  <script>
    if(!window._gaq)window._gaq=[];
    _gaq.push(['_trackEvent','Button Displayed','JBE Mini Sign Up','|PO_EC2M_EC2M',,true]);
    window.searchToJBE = function(emailAddress,formId,subType){

    $('<form action="https://www.jobsite.co.uk/cgi-bin/search_to_jbe.cgi" method="POST">'+
      '<input type="hidden" name="email" value="'+emailAddress+'">'+
      '<input type="hidden" name="skill_include" value="">'+
      '<input type="hidden" name="skill_exclude" value="">'+
      '<input type="hidden" name="skill_atleast" value="">'+
      '<input type="hidden" name="job_title_include" value="Change Manager">'+
      '<input type="hidden" name="job_title_exclude" value="">'+
      '<input type="hidden" name="job_title_atleast" value="">'+
      '<input type="hidden" name="location_include" value="PO_EC2M_EC2M">'+
      '<input type="hidden" name="location_exclude" value="">'+
      '<input type="hidden" name="location_within" value="">'+
      '<input type="hidden" name="show_desc" value="Y">'+
      '<input type="hidden" name="ignore_non_salary" value="">'+
      '<input type="hidden" name="display_ref" value="">'+
      '<input type="hidden" name="daysback" value="">'+
      '<input type="hidden" name="scc" value="">'+
      '<input type="hidden" name="search_to_jbe" value="Y">'+
      ''+
      ''+
      '<input type="hidden" name="class_name" value="">'+
      '<input type="hidden" name="channel_code" value="">'+
      '<input type="hidden" name="reqd_salary" value="">'+
      '<input type="hidden" name="search_currency_code" value="">'+
      '<input type="hidden" name="search_single_currency_flag" value="">'+
      '<input type="hidden" name="search_salary_type" value="">'+
      '<input type="hidden" name="search_salary_low" value="">'+
      '<input type="hidden" name="search_salary_high" value="">'+
      '<input type="hidden" name="channel_code_ind" value="">'+
      '<input type="hidden" name="channel_code_sub_ind" value="">'+
      '<input type="hidden" name="channel_code_func" value="">'+
      '<input type="hidden" name="channel_code_sub_func" value="">'+
      '<input type="hidden" name="jbe_add_time" value="1482327419">'+
      '<input type="hidden" name="lr" value="">'+
      '<input type="hidden" name="compare_resolved" value="">'+
      '<input type="hidden" name="compare_search" value="PO_EC2M_EC2M">'+
      ''+
      ''+
      ''+
      '<input type="hidden" name="channel_page_code" value="">'+
      '<input type="hidden" name="channel_page_method" value="">'+
      '<input type="hidden" name="from_min_jobalert" value="1">'+
      '<input type="hidden" name="min_jobalert_form" value="'+formId+'">'+
      '<input type="hidden" name="'+subType+'" value="1">'+
      '</form>').appendTo('body').submit();
    }
  </script>
  <script src="/js/min_job_alert.js"></script>

<script type="text/javascript"  async="true">
  (function() {
    var cachebust = (Math.random() + "").substr(2);
    var protocol = "https:" == document.location.protocol ? "https:" : "http:";
    new Image().src =protocol+"//20660875p.rfihub.com/ca.gif?rb=18545&ca=20660875&ra="+cachebust+"&t=view&pid=957209288"
  })();
</script>


<script type="text/javascript">
    var protocol = "https:" == document.location.protocol ? "https:" : "http:";
    var host = window.location.hostname;
    var url = protocol+"//"+host+"/cgi-bin/vacancy_hit.cgi?vac_ref=957209288&timestamp=1482327416&hash=17d2cd8dce2f75e3c7185fccd838353e";
    xmlhttp = new XMLHttpRequest();
    xmlhttp.open("GET", url, true);
    xmlhttp.send();
</script>



<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-55b62f8351ff6625" async="async"></script>

<script src="/js/react/0.14.7/react.min.js"></script>
<script src="/js/react/0.14.7/react-dom.min.js"></script>

<script src="/js/jquery.dotdotdot.js"></script>
<script src="/candidate/component/js/jobStrip.bundle.js" charset="utf-8"></script>

<div class="modal-overlay saved-jobs-modal modal fade in" aria-hidden="false">
  <div class="modal-container">
    <span class="close">x</span>
    <h3>Saved Jobs</h3>
    <p>To save jobs you must be registered with Jobsite.</p>
    <p>Existing users can <a href="/cgi-bin/login_applicant.cgi?destination=shortlist">login here</a>
    <br>or
    <br>New users can <a href="/cgi-bin/mj-account.cgi?destination=shortlist">register.</a></p>
    <p>Saving jobs enables you to take them with you across mobile, tablet and desktop - making it easier and convenient for you to apply later.</p>
  </div>
</div>

    

    
      <script defer src="/js/vacancy_tracking.js"></script>
    
  
<script>_satellite.pageBottom();</script>

</body>
</html>
