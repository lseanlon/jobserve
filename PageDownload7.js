var page = require('webpage').create();
var loadInProgress = false;
var currentStep = 0;
var system = require('system');
var maxScroll = 1;
var currentScroll = 1;

if (system.args.length === 1) {
    //-console.log('Usage: google.js <some Query>');
    phantom.exit(1);
} else {
    q = system.args[1];
    maxScroll = system.args[2];
}


// Route "console.log()" calls from within the Page context to the main Phantom context (i.e. current "this")
page.onConsoleMessage = function(msg) {
    console.log(msg);
};

page.onAlert = function(msg) {
    console.log('alert!!> ' + msg);
};

page.onLoadStarted = function() {
    loadInProgress = true;
    console.log("load started");
};

page.onLoadFinished = function(status) {
    loadInProgress = false;
    if (status !== 'success') {
        console.log('Unable to access network');
        phantom.exit();
    } else {
        console.log("load finished");
    }
};

var steps = [
    function() {

                console.log( "<html>");
        page.open(q);
    },

    function() {
        console.log('Answers:');
        page.injectJs("jquery1-11-1min.js");
        // page.render('example.png');


        setTimeout(function() {


            page.sendEvent('keypress', page.event.key.keydown);

            page.render('screenshot.png');
            page.evaluate(function() {

                $('.dragger').eq(1)[0].click();
                var e = $.Event("keydown");
                e.which = 40; // # Some key code value

                var curjobid = $('.jobSelected').parent().attr('id');
                var htmldetail = "<div class='details-" + curjobid + "'> </div>" + $('#jobdisplaypanel').html() + "</div>"
    
                console.log( $('body').html() );             
                console.log(htmldetail);
                $("input").trigger(e);

            });



        }, 3200);
    },
    function() {

                console.log( "</html>");
        page.injectJs("jquery1-11-1min.js");

        setTimeout(function() {
            page.evaluate(function() {
                console.log('Exiting');
                console.log('html: ' + $('body').html());
            });
        }, 2200);
    }
];

interval = setInterval(function() {
    if (!loadInProgress && typeof steps[currentStep] == "function") {
        console.log("step " + (currentStep + 1));
        steps[currentStep]();
        currentStep++;

        if (currentScroll <= maxScroll) {
            if (currentStep == 2) {
                currentStep--;
            }
            currentScroll++;
        }

    }
    if (typeof steps[currentStep] != "function") {
        console.log("test complete!");
        phantom.exit();
    }
}, 6250);
