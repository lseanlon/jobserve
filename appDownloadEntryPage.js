var cheerio = require('cheerio');
var fs = require('fs');
var download = require('download');
var mkdirp = require('mkdirp');

require('shelljs/global');
var downloadPath = fs.readFileSync('./src/configuration/EntryPathInfo.txt', 'utf8');
var outputJson = {};
var getUrlEntry = function() {

    var paramValue = './src/configuration/UrlList.txt';
    process.argv.forEach(function(val, index, array) { 

        //map file name
        if (index == 2 && val) {
            paramValue = val;
        }

    });

    return paramValue;
};

var urlEntry = fs.readFileSync(getUrlEntry(), 'utf8');
urlList = urlEntry.split("\n");

//timer in second
var timerWait = fs.readFileSync('./src/configuration/TimerInfo.txt', 'utf8');
timerWait = parseInt(timerWait) * 1000;


var downloadedEntryPathList = [];
//download all the first page listing
urlList.forEach(function(_elem, index, collection) {

    if(!_elem){
        return ;
    }
    
    var urlInfo = _elem.split("|");
    var urlPath = urlInfo[0];
    var urlLink = urlInfo[1];
    var ref = urlInfo[2];

    var curRow = { 'ref': ref, 'urlLink': urlLink, 'urlPath': urlPath, 'downloadPath': downloadPath };
    console.log('curRow',curRow)

    //add random number to timer
    var randomNum = Math.floor(Math.random() * 574) + 1;

    if (fs.existsSync(urlPath)) {
        randomNum = -3000;
    }

    mkdirp( "./dist/EntryPage"  , function(err) {
        if (err) return cb(err);
        console.log("create folder path",   "./dist/EntryPage" );
    });

    //add timeout - reduce suspicious of crawling
    setTimeout(function() {

        console.log('Check exist ');
        // if ( fs.existsSync(urlPath)) {
        //     console.log('File downloaded found.Skipping', urlPath);
        // } else {  }

        console.log('start download ', urlLink); 

        var commandRun = 'phantomjs PageDownload6.js "' + (urlLink) + '"  ';
        try {

            exec(commandRun, function(status, output) {
                console.log('Exit status:', status);
                console.log('Program output:');

                console.log('done download urlLink', urlLink);
                console.log('done download urlPath', urlPath);



                if (!urlPath || !output) {
                    return;
                }

                fs.writeFileSync(urlPath, output);
                handleSubPages(curRow);


            });

        } catch (e) {
            console.log(e);

        }
        downloadedEntryPathList.push(curRow);



    }, index * timerWait   + randomNum);

});

var handleSubPages = function(_elem) {


    console.log('handleSubPages');
    //each first page lisitng, download al the sub pages 
    var randomNum = null;

    //add random number to timer
    randomNum = Math.floor(Math.random() * 574) + 1;


    var fileContent = fs.readFileSync(_elem.urlPath, 'utf8');

    var $ = cheerio.load(fileContent);




    // determine record per page
    var listingPerPage = 25;
    console.log('listingPerPage', listingPerPage);

    //determine number of record
    var totalrec =  1
    if($('.resultnumber')){ 
        totalrec =  $('.resultnumber').html() 
        totalrec = parseFloat(totalrec) 
        totalrec= isNaN(totalrec) ? 0:totalrec;  
    }

    console.log('totalrec', totalrec);


    //divide no of record over pagination, use ceiling
    var numberOfPage = totalrec  /listingPerPage ;
    numberOfPage = Math.ceil(numberOfPage);
     console.log('numberOfPage', numberOfPage);

    


    var filePathFormat = _elem.downloadPath + "pages-" + _elem.ref;
    outputJson = { "originalLink":  _elem.urlLink , "numberOfPage": numberOfPage, "ref": _elem.ref, "filePathFormat": filePathFormat };
    fs.appendFileSync('./dist/listingIndex.txt', JSON.stringify(outputJson) + ",");

    //download all the page 
    downloadAllListPages(1, totalrec,_elem, timerWait + randomNum)
     



};
function removeURLParameter(url, parameter) {
    //prefer to use l.search if you have a location/link object
    var urlparts= url.split('?');   
    if (urlparts.length>=2) {

        var prefix= encodeURIComponent(parameter)+'=';
        var pars= urlparts[1].split(/[&;]/g);

        //reverse iteration as may be destructive
        for (var i= pars.length; i-- > 0;) {    
            //idiom for string.startsWith
            if (pars[i].lastIndexOf(prefix, 0) !== -1) {  
                pars.splice(i, 1);
            }
        }

        url= urlparts[0]+'?'+pars.join('&');
        return url;
    } else {
        return url;
    }
}
 
function downloadAllListPages(i, _totalrec,_elem, timerWait) {
    // _elem.urlLink = _elem.urlLink.replace("&p=", "");
    // var urlLinkPage = _elem.urlLink + "&p=2" + i;    
     _elem.urlLink = removeURLParameter(_elem.urlLink , 'p')
    var urlLinkPage = _elem.urlLink ;  
    //already has param string
    // if (_elem.urlLink && (_elem.urlLink.indexOf("?") > -1 || _elem.urlLink.indexOf("&") > -1)) {
    //     urlLinkPage  = urlLinkPage + "&p=" + i;
    // } else {
    //     urlLinkPage  = urlLinkPage + "?p=" + i;
    // } 
 
    var urlLinkDownloadPage = _elem.downloadPath + "pages-" + _elem.ref  + ".txt";
    mkdirp(_elem.downloadPath, function(err) {
        if (err) return cb(err);
        console.log("create folder path", _elem.downloadPath);
    });

    var newnum = 0;
    if (fs.existsSync(urlLinkDownloadPage)) {
        newnum = -3000;
    }

    setTimeout(function() {
        if (fs.existsSync(urlLinkDownloadPage)) {
            console.log('File downloaded found. Skipping download', urlLinkPage);
        } else {
            console.log('urlLinkDownloadPage', urlLinkDownloadPage);
            console.log('urlLinkPage', urlLinkPage);
            console.log(' _elem.downloadPath', _elem.downloadPath); 
       

            try { 
                var commandRun = 'phantomjs PageDownload7.js "' + (urlLinkPage) + '" '+_totalrec + '';
                console.log('commandRun',commandRun);
                exec(commandRun, function(status, output) {
                    console.log('Exit status:', status);
                    console.log('done download - ' + _elem.urlLink + ' into ' + _elem.urlPath);

                    if (!urlLinkDownloadPage || !output) {
                        return;
                    }

                    fs.writeFileSync(urlLinkDownloadPage, output);


                });

            } catch (e) {
                console.log(e);

            }
 
        }
    }, i * 3000 - newnum);
}
