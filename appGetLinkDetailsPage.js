var cheerio = require('cheerio');
var fs = require('fs');
var download = require('download');

var jsonListIndexStr = "[" + fs.readFileSync("./dist/listingIndex.txt", 'utf8') + "{}]";
var jsonIndexList = JSON.parse(jsonListIndexStr);

var downloadPath = fs.readFileSync('./src/configuration/EntryPathInfo.txt', 'utf8');
var outputJson = {};

var ref = '';
//timer in second
var timerWait = fs.readFileSync('./src/configuration/TimerInfo.txt', 'utf8');
timerWait = parseInt(timerWait) * 1000;

//get config
//loop config, 
// loop indexes

var listOfLink = {}

var finalResultList = [];
var finalResultExcelList = [];
var downloadedEntryPathList = [];
//download all the first page listing
jsonIndexList.forEach(function(_elem, index, collection) {

    var randomNum = Math.floor(Math.random() * 574) + 1;

    if (!_elem || !_elem.numberOfPage) {
        return;
    }

    var i = 0;
    addAllLinkInformation(i, _elem, timerWait + randomNum, i == _elem.numberOfPage);


});




function addAllLinkInformation(i, _elem, timerWait, _isLastIndex) {
    // grab file, cheerio
    // grab link html

    // setTimeout(function() { 

    var fileContent = "<html><body> " + fs.readFileSync(_elem.filePathFormat + ".txt", 'utf8') + "</body></html>";
    var $ = cheerio.load(fileContent)

    console.log(_elem.filePathFormat)
    $jobItem = $('.jobItem');;

    console.log($jobItem.html())

    $jobItem.each(function(idx, elemm) {

        $elem = $(this);

        var $jobdetails = $(".details-" + $elem.prop('id')).next();

        if (!$(".details-" + $elem.prop('id')) || !$jobdetails) {
            return;
        }

        if (!listOfLink[_elem.ref]) { listOfLink[_elem.ref] = []; }
        var temList = [];


        var jobDesc = "";
        var rawJobdesc = '';
        var isView = true;
        var BulletList = [];

        var currentRow = {};
        currentRow['originalLink'] = _elem["originalLink"];
        ref = _elem.ref;
        currentRow['ref'] = _elem.ref;

        if ($jobdetails.find("#md_ref")) {
            currentRow['jobRef'] = $jobdetails.find("#md_ref").html()
        }

        //contactName
        if ($jobdetails.find("#md_contact")) {
            currentRow['contactName'] = $jobdetails.find("#md_contact").html()
        }
        if ($jobdetails.find("#md_email a").prop('href')) {
            currentRow['contactName'] = currentRow['contactName'] + " " + $jobdetails.find("#md_email a").prop('href')
        }


        //company nmae 
        currentRow['company'] = ''


        if ($jobdetails.find("[itemprop='hiringOrganization']")) {
            currentRow['company'] = $jobdetails.find("[itemprop='hiringOrganization']").html()
        }


        //description 
        jobDesc = "";
        rawJobdesc = $jobdetails.find("#md_skills").html();
        currentRow['jobDesc'] = rawJobdesc;

        //filter out bullet lists 
        $jobdetails.find("#md_skills").find('li').each(function(ii, element) {
            BulletList.push($jobdetails.find(element).html());
        });

        //area 
        currentRow['area'] = "";

        if ($jobdetails.find("#td_location_salary")) {
            currentRow['area'] = $jobdetails.find("#td_location_salary").html()
        }




        //salary 
        var salary = '';
        if ($jobdetails.find("#md_rate")) {
            currentRow['salary'] = $jobdetails.find("#md_rate").html()

        }


        currentRow['salary'] = unescape(salary);
        currentRow['salary'] = currentRow['salary'].replace('&#xA3;', 'Pound').replace('&#xA3;', 'Pound');



        //emptype - contract -etc 
        currentRow['jobType'] = ''

        if ($jobdetails.find(".td_job_type")) {
            currentRow['jobType'] = $jobdetails.find(".td_job_type").html()
        }


        if ($jobdetails.find("#td_jobpositionlink")) {
            currentRow['title'] = $jobdetails.find("#td_jobpositionlink").html()
        }



        currentRow['keyword1'] = '';
        currentRow['keyword2'] = '';
        currentRow['keyword3'] = '';

        if (rawJobdesc) {
            while (rawJobdesc.indexOf("*") != -1) {
                rawJobdesc = rawJobdesc.replace("*", "\[asterisk\]");
            }
        }

        if (BulletList.length) {
            if (BulletList[0]) {
                currentRow['keyword1'] += '  '
                currentRow['keyword1'] += BulletList[0]
            }
            if (BulletList[1]) {
                currentRow['keyword1'] += '  '
                currentRow['keyword1'] += BulletList[1]
            }
            if (BulletList[2]) {
                currentRow['keyword2'] += '  '
                currentRow['keyword2'] += BulletList[2]
            }
            if (BulletList[3]) {
                currentRow['keyword2'] += '  '
                currentRow['keyword2'] += BulletList[3]
            }
            if (BulletList[4]) {
                currentRow['keyword3'] += '  '
                currentRow['keyword3'] += BulletList[4]
            }
            if (BulletList[5]) {
                currentRow['keyword3'] += '  '
                currentRow['keyword3'] += BulletList[5]
            }
        } else {
            if (rawJobdesc) {
                rawJobdesc = rawJobdesc.replace(/\<br\>o /gi, '[listbullet]')
                rawJobdesc = rawJobdesc.replace(/\<br\>·/gi, '[listbullet]')
                rawJobdesc = rawJobdesc.replace(/\<br\>- /gi, '[listbullet]')
                    // rawJobdesc=rawJobdesc.replace(/\<br\>· /gi,'[listbullet]') 
                rawJobdesc = rawJobdesc.replace(/\[asterisk\]/gi, '[listbullet]')
            } else {
                rawJobdesc = " ";
            }

            //filter out Dash lists
            // 
            var DashList = rawJobdesc.split('[listbullet]')


            var sanitizeDash = function(_val) {
                //take out all html   
                return ($("<p>" + _val + "</p>").text());
            }
            DashList = DashList.map(sanitizeDash);

            if (DashList.length) {
                if (DashList[0]) {
                    currentRow['keyword1'] += '  '
                    currentRow['keyword1'] += DashList[0]
                }
                if (DashList[1]) {
                    currentRow['keyword1'] += '  '
                    currentRow['keyword1'] += DashList[1]
                }
                if (DashList[2]) {
                    currentRow['keyword1'] += '  '
                    currentRow['keyword1'] += DashList[2]
                }
                if (DashList[3]) {
                    currentRow['keyword2'] += '  '
                    currentRow['keyword2'] += DashList[3]
                }
                if (DashList[4]) {
                    currentRow['keyword2'] += '  '
                    currentRow['keyword2'] += DashList[4]
                }
                if (DashList[5]) {
                    currentRow['keyword2'] += '  '
                    currentRow['keyword2'] += DashList[5]
                }
                if (DashList[6]) {
                    currentRow['keyword3'] += '  '
                    currentRow['keyword3'] += DashList[6]
                }
                if (DashList[7]) {
                    currentRow['keyword3'] += '  '
                    currentRow['keyword3'] += DashList[7]
                }
                if (DashList[8]) {
                    currentRow['keyword3'] += '  '
                    currentRow['keyword3'] += DashList[8]
                }
            }

        }




        String.prototype.replaceAll = function(search, replacement) {
            var target = this;
            return target.replace(new RegExp(search, 'g'), replacement);
        };
        currentRow['keyword1'] = currentRow['keyword1'].replace(/  /gi, '').replace('*', '').replace('*', '').replace('*', '')
        currentRow['keyword2'] = currentRow['keyword2'].replace(/  /gi, '').replace('*', '').replace('*', '').replace('*', '')
        currentRow['keyword3'] = currentRow['keyword3'].replace(/  /gi, '').replace('*', '').replace('*', '').replace('*', '')

        currentRow['keyword1'] = ($("<p>" + currentRow['keyword1'] + "</p>").text())
        currentRow['keyword2'] = ($("<p>" + currentRow['keyword2'] + "</p>").text())
        currentRow['keyword3'] = ($("<p>" + currentRow['keyword3'] + "</p>").text())


        if (currentRow['keyword1'] && currentRow['keyword1'].length > 50) {
            currentRow['keyword1'] = currentRow['keyword1'].substring(0, 50);
        }

        if (currentRow['keyword2'] && currentRow['keyword2'].length > 50) {
            currentRow['keyword2'] = currentRow['keyword2'].substring(0, 50);
        }

        if (currentRow['keyword3'] && currentRow['keyword3'].length > 50) {
            currentRow['keyword3'] = currentRow['keyword3'].substring(0, 50);
        }


        console.log("currentRow ", currentRow);
        if (!currentRow.jobDesc || !currentRow['keyword1']) {
            return;
        }

        //prevent duplicate  

        var isExist = false;
        for (var x = 0; x < finalResultList.length; x++ ) {
            var curitem = finalResultList[x];
            if (curitem.jobRef == currentRow.jobRef) {
                isExist = true ;
                return false;
            }

        }

        if (!finalResultExcelList[ref]) { finalResultExcelList[ref] = []; }
        if (!isExist) { 
            finalResultList.push(currentRow);
            finalResultExcelList[ref].push(currentRow);
        }





    });
    _isLastIndex = true;
    if (_isLastIndex) {}
    fs.writeFileSync("./dist/detailLinkIndex.txt", JSON.stringify(listOfLink));


    var filepath = './dist/recordFinalDetails-' + ref + '.txt'
    console.log("Write to file ", filepath);
    fs.writeFileSync(filepath, JSON.stringify(finalResultList));
    // }, i * 3000);
}

//write into excel 
var json2xls = require('json2xls');
for (var key in finalResultExcelList) {
    if (finalResultExcelList.hasOwnProperty(key)) {
        // console.log(key + " -> " + finalResultExcelList[key]);  
        var xls = json2xls(finalResultExcelList[key]);
        var outexcelpath = './dist/' + key + '-data.xlsx';
        console.log("Write to excel ", outexcelpath);
        fs.writeFileSync(outexcelpath, xls, 'binary');
    }
}
